var express = require('express');
var router = express.Router();
var title = 'Modeso Web Chat'

/* GET home page. */



router.get('/', function (req, res, next) {

  if (!req.cookies.userCookie) {
    if (!req.session.userSession) {
      loggedIn = false
    } else {
      user = req.cookies.userCookie
      loggedIn = true
    }
  } else {
    loggedIn = true
    user = req.cookies.userCookie
 
  }
  res.render('index', {title,
    loggedIn:(typeof(user)!='undefined')?true:false,
    user:(typeof(user)!='undefined')?user:null
  });

});

module.exports = router;