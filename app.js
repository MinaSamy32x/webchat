var createError = require('http-errors');
var express = require('express');
var path = require('path');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var socket=require('socket.io');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/{webchat}');
mongoose.connection.once('open',()=>{
  console.log('connected to mongoo')
})
mongoose.connection.on('error',(err)=>{
  console.log('Error while connecting to mongo',err)
})

var app = express();
app.use(session({ 
  secret: 'mysecret',
  resave: true,
  saveUninitialized: false, 
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
  cookie: { maxAge: 30 * 60 * 1000 } 
}))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
