var express = require('express');
var router = express.Router();
users = require('../models/users');
var title = 'Modeso Web Chat'
var loggedIn;
var loginError;
var user = {
  name: ''
};
var home = false


/* GET users listing. */

function requireLogin(req, res, next) {
  if (!req.cookies.userCookie) {
    if (!req.session.userSession) {
      res.render('login', {
        title,
        loggedIn: true,
        user
      })
    } else {
      res.redirect('/index')
    }
  } else {
    next();
  }

};

router.get('/chat', requireLogin, function (req, res, next) {
  res.render('chat', {
    users: users,
    title,
    user:req.cookies.userCookie,
  });
});


router.get('/register', function (req, res) {
  if (!req.session.userSession)
    res.render('register', {
      title,
      loggedIn: false,
      form_type: 'register'
    })
  else {
    res.redirect('/index')
  }
})


router.post('/register', function (req, res) {
  var user = new User();
  user.name = req.body.Username
  user.email = req.body.Email
  user.password = req.body.Password
  user.save(function (err, doc) {
    if (err) {
      throw err
    }

    req.session.userSession = req.body.Username
    current_user = user
    res.cookie('userCookie', current_user)

    res.render('index', {
      title,
      loggedIn: true,
      user: user
    })
  })
})


router.get('/login', function (req, res, next) {
  if (!req.cookies.userCookie) {
    if (!req.session.userSession) {
      res.render('login', {
        title,
        loggedIn: false
      })
    } else {
      res.redirect('/index')
    }
  } else {
    res.render('index', {
      title,
      loginError: false,
      loggedIn: true,
      user: req.cookies.userCookie
    })
  }
});

router.post('/login', function (req, res, next) {
  loginFlag = false
  User.find(function (err, docs) {
    if (err) {
      console.log(err)
    } else {
      allusers = docs
    }
  }).then(function (e) {
    allusers.forEach(function (user) {
      if (req.body.Username == user.name && req.body.Password == user.password) {
        loginFlag = true
        req.session.userSession = req.body.Username
        user_data = {
          name: user.name,
          password: user.password,
          email: user.email,
          id: user.id
        }
        res.cookie('userCookie', user_data)
        res.render('index', {
          title,
          loggedIn: true,
          user: user
        })
        console.log('session : ' + req.session.userSession)
        console.log('cookie Username: ' + req.cookies.userCookie.name)
        console.log('cookie User Password: ' + req.cookies.userCookie.password)
      }
    });
  }).then(function (e) {
    if (loginFlag == false) {
      res.render('login', {
        title,
        loginError: true,
        loggedIn: false,
        user: user
      })
    }
  })

  // HEREEE CHANGE DATA FROM ARRAY TO DATABASE

});

router.get('/logout', function (req, res) {
  res.clearCookie("userCookie");
  req.session.userSession = null
  res.render('index', {
    title,
    loggedIn: false
  })
});


router.get('/profile/:id', function (req, res) {

  User.find(function (err, docs) {
    if (err) {
      console.log(err)
    } else {
      allusers = docs
    }
  }).then(function (e) {

    User.findOne({_id:req.params.id}).exec(function(err,profile){
      console.log(profile)
    res.render('profile', {
      title,
      profile,
      allusers
    })
  })
  }).catch(function (error) {
    console.log(error);
  });
  
})

router.post('/addcontanct/:contactid', function (req, res) {
  User.findOne({_id:req.params.contactid}, function (err, user){
   User.findByIdAndUpdate({ '_id':req.cookies.userCookie.id },  {
    $push:{ contacts: user.id}
  }, function (err, doc) {
    res.redirect('/users/profile/'+req.cookies.userCookie.id)
  })
  })
  
})



module.exports = router;